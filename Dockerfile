
# https://www.artecat.ch/jexler/

# By default, the container will run as user "jexler" in group "root" on plain Docker.
# On OpenShift, the user is random (mandated by OpenShift) and also in the group "root".
# Therefore all files/dirs that need to be writable by the respective user are given write
# access for group "root". Note also that on OpenShift there is no user home directory.

FROM tomcat:jre8-alpine

ENV JEXLER_VERSION=4.0.1
ENV JEXLER_HOME=/jexler
ENV JEXLER_JEXLERS=$JEXLER_HOME/jexlers

ENV JEXLER_WEBAPP_DIR=$CATALINA_HOME/webapps/ROOT
ENV JEXLER_WEBAPP_JEXLERS_DIR=$JEXLER_WEBAPP_DIR/WEB-INF/jexlers

USER root

# Create user "jexler" in group "root"
RUN adduser -H -D -S jexler -G root

# Create jexler base directory
RUN X=$JEXLER_HOME && mkdir $X && chown -R jexler:root $X

# Create and configure Grape related stuff
RUN X=$JEXLER_HOME/grape && mkdir $X && chown -R jexler:root $X && chmod -R g+w $X
ENV JAVA_OPTS="$JAVA_OPTS -Dgrape.root=/jexler/grape/grapes"
RUN X=$JEXLER_HOME/grape/grapes && mkdir $X && chown -R jexler:root $X && chmod -R g+w $X
ENV JAVA_OPTS="$JAVA_OPTS -Dgrape.config=/jexler/grape/grapeConfig.xml"
ADD grape/grapeConfig.xml $JEXLER_HOME/grape/grapeConfig.xml
RUN X=$JEXLER_HOME/grape/grapeConfig.xml && chown -R jexler:root $X
ADD grape/defaultGrapeConfig.xml $JEXLER_HOME/grape/defaultGrapeConfig.xml
RUN X=$JEXLER_HOME/grape/defaultGrapeConfig.xml && chown -R jexler:root $X

# Give write access to Tomcat temp directory
# (needed by Grape, or by Jexler Groovy scripts that would use common Java calls to get a temp file/dir)
RUN X=$CATALINA_HOME/temp && chown -R jexler:root $X && chmod -R g+w $X

# Download and unzip the jexler JAR to ROOT webapp
RUN rm -rf $CATALINA_HOME/webapps/*
RUN mkdir $JEXLER_WEBAPP_DIR
RUN wget -q https://sourceforge.net/projects/jexler/files/jexler-$JEXLER_VERSION.war -O /tmp/jexler.war
RUN unzip -q -o /tmp/jexler.war -d $JEXLER_WEBAPP_DIR
RUN rm /tmp/jexler.war
RUN X=$JEXLER_WEBAPP_DIR && chown -R jexler:root $X

# Move jexlers dir to $JEXLER_JEXLERS, make writeable, create symlink back
RUN mv $JEXLER_WEBAPP_JEXLERS_DIR $JEXLER_JEXLERS
RUN X=$JEXLER_JEXLERS && chown -R jexler:root $X && chmod -R g+w $X
RUN ln -s $JEXLER_JEXLERS $JEXLER_WEBAPP_JEXLERS_DIR

USER jexler
